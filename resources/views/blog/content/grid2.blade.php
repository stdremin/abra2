<section class="module">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="row multi-columns-row post-columns">
                    <?/* @var \App\Entity\Blog\Post $post*/?>
                    @foreach($data as $post)
                        <div class="col-md-6 col-lg-6">
                            <div class="post">
                                <div class="post-thumbnail">
                                    <a href="#">
                                        <img src="{{ asset('assets/images/post-1.jpg') }}" alt="Blog-post Thumbnail" />
                                    </a>
                                </div>
                                <div class="post-header font-alt">
                                    <h2 class="post-title"><a href="#">{{ $post->name }}</a></h2>
                                    <div class="post-meta">By&nbsp;<a href="#">Mark Stone</a>&nbsp;| 23 November | 3 Comments
                                    </div>
                                </div>
                                <div class="post-entry">
                                    <p>{{ $post->preview_text }}</p>
                                </div>
                                <div class="post-more"><a class="more-link" href="#">Подробности</a></div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="pagination font-alt">
                    <a href="#"><i class="fa fa-angle-left"></i></a><a class="active" href="#">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#"><i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
                @include('blog/parts/sidebar')
            </div>
        </div>
    </div>
</section>