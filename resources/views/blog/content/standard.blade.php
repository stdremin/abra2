@yield('main_banner')
<section class="module">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                @yield('items')
            </div>
            <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
                @include('blog/parts/sidebar', ['sections' => $sections, 'populars' => $populars])
            </div>
        </div>
    </div>
</section>