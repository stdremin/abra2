@extends('blog.list')
@section('main_banner')
    <section class="module bg-dark-60 blog-page-header" data-background="assets/images/blog_bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h2 class="module-title font-alt">Blog Grid</h2>
                    <div class="module-subtitle font-serif">
                        A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring
                        which I enjoy with my whole heart.
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('items')
    <div class="row multi-columns-row post-columns">
		<?/* @var \App\Entity\Blog\Post $post */?>
        @foreach($data as $post)
            <div class="post">
                <div class="post-thumbnail">
                    <a href="{{ route('post.detail', ['section_code' => $post->sectionCode->code, 'code' => $post->code]) }}">
                        <img src="{{ $post->getSmallImage() }}" alt="{{ $post->name }}" />
                    </a>
                </div>
                <div class="post-header font-alt">
                    <h2 class="post-title">
                        <a href="{{ route('post.detail', ['section_code' => $post->sectionCode->code, 'code' => $post->code]) }}">
                            {{ $post->name }}
                        </a>
                    </h2>
                    <div class="post-meta">
                        {{ $post->active_from }} | 3 Comments |
                        @foreach($post->tags as $tag)
                            <a href="{{ $tag['url'] }}">{{ $tag['tag'] }}</a>
                            @if (!$loop->last),@endif
                        @endforeach
                    </div>
                </div>
                <div class="post-entry">
                    <p>{!! $post->preview_text  !!}</p>
                </div>
                <div class="post-more">
                    <a class="more-link" href="{{ route('post.detail', ['section_code' => $post->sectionCode->code, 'code' => $post->code]) }}">
                        Подробности
                    </a>
                </div>
            </div>
        @endforeach
    </div>
    {{ $data->links('vendor.pagination.default') }}
@stop
@push('page-loader')
    <div class="page-loader">
        <div class="loader">Loading...</div>
    </div>
@endpush