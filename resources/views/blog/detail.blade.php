@extends('blog.list')
@section('main_banner')
    <section class="module bg-dark-60 blog-page-header" data-background="{{ $data->detailPicture->getOriginalPath() }}">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1 class="module-title font-alt">{{ $data->name }}</h1>
                    {{--<div class="module-subtitle font-serif"></div>--}}
                </div>
            </div>
        </div>
    </section>
@stop
@section('items')
	<?/* @var \App\Entity\Blog\Post $data */?>
    <div class="post">
       {{-- <div class="post-thumbnail">
            <img src="{{ $data->getSmallImage() }}" alt="{{ $data->name }}"/>
        </div>--}}
        <div class="post-header font-alt">
            {{--<h1 class="post-title">{{ $data->name }}</h1>--}}
            <div class="post-meta">&nbsp;{{ $data->active_from }} | 3 Comments |
                @foreach($data->tags as $tag)
                    <a href="{{ $tag['url'] }}">{{ $tag['tag'] }}</a>,
                @endforeach
            </div>
        </div>
        <div class="post-entry">{!! $data->detail_text !!}</div>

        @if($data->property->source_link !== '')
            <div class="detail--source-block">
                <b>Источник: </b>
                <a href="{{ $data->property->source_link }}" target="_blank">
                    @if($data->property->source_title !== '')
                        {{ $data->property->source_title }}
                    @else
                        {{ $data->property->source_link }}
                    @endif
                </a>
            </div>
        @endif

        @if($data->property->links_post->count() > 0)
            @include('blog.parts.link_post', ['posts' => $data->property->links_post])
        @endif
        {{--<div class="comments">
            <h4 class="comment-title font-alt">There are 3 comments</h4>
            <div class="comment clearfix">
                <div class="comment-avatar"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/ryanbattles/128.jpg" alt="avatar"/></div>
                <div class="comment-content clearfix">
                    <div class="comment-author font-alt"><a href="#">John Doe</a></div>
                    <div class="comment-body">
                        <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The European languages are members of the same family. Their separate existence is a myth.</p>
                    </div>
                    <div class="comment-meta font-alt">Today, 14:55 - <a href="#">Reply</a>
                    </div>
                </div>
                <div class="comment clearfix">
                    <div class="comment-avatar"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/draganbabic/128.jpg" alt="avatar"/></div>
                    <div class="comment-content clearfix">
                        <div class="comment-author font-alt"><a href="#">Mark Stone</a></div>
                        <div class="comment-body">
                            <p>Europe uses the same vocabulary. The European languages are members of the same family. Their separate existence is a myth.</p>
                        </div>
                        <div class="comment-meta font-alt">Today, 15:34 - <a href="#">Reply</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comment clearfix">
                <div class="comment-avatar"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/pixeliris/128.jpg" alt="avatar"/></div>
                <div class="comment-content clearfix">
                    <div class="comment-author font-alt"><a href="#">Andy</a></div>
                    <div class="comment-body">
                        <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The European languages are members of the same family. Their separate existence is a myth.</p>
                    </div>
                    <div class="comment-meta font-alt">Today, 14:59 - <a href="#">Reply</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="comment-form">
            <h4 class="comment-form-title font-alt">Add your comment</h4>
            <form method="post">
                <div class="form-group">
                    <label class="sr-only" for="name">Name</label>
                    <input class="form-control" id="name" type="text" name="name" placeholder="Name"/>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="email">Name</label>
                    <input class="form-control" id="email" type="text" name="email" placeholder="E-mail"/>
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="comment" name="comment" rows="4" placeholder="Comment"></textarea>
                </div>
                <button class="btn btn-round btn-d" type="submit">Post comment</button>
            </form>
        </div>--}}
    </div>

@endsection
@push('detail_blog_scripts')
    <link href="{{ asset('js/code/prism.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/code/prism.js') }}"></script>
@endpush