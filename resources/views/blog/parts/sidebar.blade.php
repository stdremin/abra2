<div class="widget">
    <form role="form">
        <div class="search-box">
            <input class="form-control" type="text" placeholder="Search..." />
            <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </form>
</div>
<div class="widget">
    <h5 class="widget-title font-alt">Разделы блога</h5>
    <ul class="icon-list">
        @foreach($sections as $section)
            <li><a href="{{ url('/blog', ['code' => $section->code]) }}">{{ $section->name }}</a></li>
        @endforeach
            <li><a href="{{ url('/blog') }}">Все разделы</a></li>
    </ul>
</div>
@if($populars)
    <div class="widget">
        <h5 class="widget-title font-alt">Популярные посты</h5>
        <ul class="widget-posts">
            @foreach ($populars as $item)
                <li class="clearfix">
                    <div class="widget-posts-image">
                        <a href="{{ route('post.detail', ['section_code' => $item->sectionCode->code, 'code' => $item->code]) }}">
                            <img src="{{ $item->preview_picture->getResizedImg() }}" alt="{{ $item->name }}" />
                        </a>
                    </div>
                    <div class="widget-posts-body">
                        <div class="widget-posts-title">
                            <a href="{{ route('post.detail', ['section_code' => $item->sectionCode->code, 'code' => $item->code]) }}">
                                {{ $item->name }}
                            </a>
                        </div>
                        <div class="widget-posts-meta">{{ $item->active_form }}</div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endif

<div class="widget">
    <h5 class="widget-title font-alt">Tag</h5>
    <div class="tags font-serif">
        @foreach($cloudTags as $tag)
            <a href="{{ route('section.tag', ['tag' => $tag->code]) }}" rel="tag">{{ $tag->tag }}</a>
        @endforeach
    </div>
</div>
<div class="widget">
    <h5 class="widget-title font-alt">Text</h5>The languages only differ in their grammar, their pronunciation and their most common
    words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.
</div>
{{--
<div class="widget">
    <h5 class="widget-title font-alt">Recent Comments</h5>
    <ul class="icon-list">
        <li>Maria on <a href="#">Designer Desk Essentials</a></li>
        <li>John on <a href="#">Realistic Business Card Mockup</a></li>
        <li>Andy on <a href="#">Eco bag Mockup</a></li>
        <li>Jack on <a href="#">Bottle Mockup</a></li>
        <li>Mark on <a href="#">Our trip to the Alps</a></li>
    </ul>
</div>--}}
