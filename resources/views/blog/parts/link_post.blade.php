<section class="module link-post">
        <h4 class="title-block">Читайте также</h4>
        <div class="row multi-columns-row post-columns">
			<?/** @var \App\Entity\Blog\Post $postLink */?>
            @foreach($posts as $postLink)
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail">
                            <a href="{{ route('post.detail', ['section_code' => $postLink->sectionCode->code, 'code' => $postLink->code]) }}">
                                <img src="{{ $postLink->getSmallImage() }}" alt="{{ $postLink->name }}"/>
                            </a>
                        </div>
                        <div class="post-header font-alt">
                            <h4 class="post-title-link">
                                <a href="{{ route('post.detail', ['section_code' => $postLink->sectionCode->code, 'code' => $postLink->code]) }}">
                                    {{ $postLink->name }}
                                </a>
                            </h4>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
</section>