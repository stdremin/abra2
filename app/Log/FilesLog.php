<?php
/**
 * Created by OOO 1C-SOFT.
 * User: GrandMaster
 * Date: 15.09.18
 */

namespace App\Log;

use Illuminate\Support\Facades\File;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class FilesLog
{
	protected $logger;
	protected $file = '';

	public function __construct($fileName = '')
	{
		$this->file = $fileName ? : __DIR__.'/../../storage/logs/import_log.log';
		$this->logger = new Logger('import_abra');
		$stream = new StreamHandler($this->file);
		$stream->setFormatter(new TraceDecorator());

		$this->logger->pushHandler($stream);
//		$this->logger->pushHandler(new FirePHPHandler());

	}


	public function log($data, $message = '')
	{
		$this->logger->debug($message."\r\n".print_r($data, 1));
	}

	public function deleteFile()
	{
		File::delete($this->file);
	}

	/**
	 * @method getFile - get param file
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * @method setFile - set param File
	 * @param string $file
	 */
	public function setFile($file)
	{
		$this->file = $file;
	}

	/**
	 * @method toLog
	 * @param bool $data
	 */
	public static function toLog($data = false)
	{
		$logger = new static(__DIR__.'/../../storage/logs/import_log.log');
		$logger->log($data);
	}
}
