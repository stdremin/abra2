<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 17.09.2018
 */

namespace App\Log;


use App\Helpers\Tools;
use Monolog\Formatter\LineFormatter;

class TraceDecorator extends LineFormatter
{
	public function __construct(?string $format = null, ?string $dateFormat = null, bool $allowInlineLineBreaks = false, bool $ignoreEmptyContextAndExtra = false)
	{

		$format = "============================================================= \n";
		$format .=  "[%datetime%] %channel%.%level_name%: \n %message% %context% \n";

		$allowInlineLineBreaks = true;
		parent::__construct($format, $dateFormat, $allowInlineLineBreaks, $ignoreEmptyContextAndExtra);
	}

	public function format(array $record)
	{
		$traceDepth = 10;
		$bShowArgs = false;

		$arBacktrace = Tools::getBackTrace($traceDepth, ($bShowArgs? null : DEBUG_BACKTRACE_IGNORE_ARGS));
		$strFunctionStack = "";
		$strFilesStack = "";
		$iterationsCount = min(count($arBacktrace), $traceDepth);
		for ($i = 1; $i < $iterationsCount; $i++)
		{
			if (strlen($strFunctionStack)>0)
				$strFunctionStack .= " < ";

			if (isset($arBacktrace[$i]["class"]))
				$strFunctionStack .= $arBacktrace[$i]["class"]."::";

			$strFunctionStack .= $arBacktrace[$i]["function"];

			if(isset($arBacktrace[$i]["file"]))
				$strFilesStack .= "\t".$arBacktrace[$i]["file"].":".$arBacktrace[$i]["line"]."\n";
			if($bShowArgs && isset($arBacktrace[$i]["args"]))
			{
				$strFilesStack .= "\t\t";
				if (isset($arBacktrace[$i]["class"]))
					$strFilesStack .= $arBacktrace[$i]["class"]."::";
				$strFilesStack .= $arBacktrace[$i]["function"];
				$strFilesStack .= "(\n";
				foreach($arBacktrace[$i]["args"] as $value)
					$strFilesStack .= "\t\t\t".$value."\n";
				$strFilesStack .= "\t\t)\n";

			}
		}

		$record['message'] = $strFunctionStack."\n".$strFilesStack.$record['message'];
//		dd($record);
		return parent::format($record);
	}

	public function formatBatch(array $records)
	{
		return parent::formatBatch($records);
	}


}