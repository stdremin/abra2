<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\SectionElement
 *
 * @property int $id
 * @property int $section_id
 * @property int $element_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SectionElement whereElementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SectionElement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SectionElement whereSectionId($value)
 * @mixin \Eloquent
 */
class SectionElement extends Model
{
    protected $table = 'dar_section_element';

	public $timestamps = false;

	protected $fillable = ['section_id', 'element_id'];
}
