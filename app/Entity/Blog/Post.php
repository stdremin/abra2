<?php

namespace App\Entity\Blog;

use App\Entity\Files;
use App\Entity\Sections;
use App\Entity\SeoData;
use App\Helpers\Transliteration;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Entity\Blog\Post
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_create
 * @property int $user_update
 * @property int|null $section_id
 * @property int $id_block
 * @property int $active
 * @property string $active_from
 * @property string $active_to
 * @property int $sort
 * @property string $name
 * @property int|null $preview_picture
 * @property int|null $detail_picture
 * @property string $preview_text
 * @property string $detail_text
 * @property string $searchable_content
 * @property string $status
 * @property string|null $xml_id
 * @property string|null $code
 * @property string|null $tags
 * @property int $counter_view
 * @property int $old
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereActiveFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereActiveTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereCounterView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereDetailPicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereDetailText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereIdBlock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereOld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post wherePreviewPicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post wherePreviewText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereSearchableContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereTags($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereUserCreate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereUserUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Post whereXmlId($value)
 * @mixin \Eloquent
 * @property-read \App\Entity\Files $detailPicture
 * @property-read \App\Entity\Files $previewPicture
 * @property-read \App\Entity\Sections $sectionCode
 */
class Post extends Model
{

	const STATUS_DRAFT = 'DRAFT';
	const STATUS_PUBLISHING = 'PUBLISHING';

	protected $table = 'dar_blog_posts';

	protected $fillable = [
		'user_create', 'user_update', 'id_block', 'section_id', 'active', 'sort', 'name', 'active_from', 'active_to',
		'preview_picture', 'detail_picture', 'preview_text', 'detail_text', 'searchable_content',
		'status', 'xml_id', 'code', 'tags', 'counter_view',
	];

	public function savePost(array $options = [], $id = null)
	{
		$search = strip_tags($options['detail_text'])." ".strip_tags($options['preview_text']);
		$search = strtoupper($search);

		$options['searchable_content'] = $search;
		$res = 0;
		if ($id){
			$element = $this->whereId($id)->first();
			if ($element->update($options)){
				$res = $element->id;
			}
		} else {
			$res = static::create($options)->id;
		}

		if ($res > 0 && strlen($options['tags'])){
			$tags = new Tags();
			$tags->saveTag($res, $options['tags']);
		}

		return $res;
	}

	public function addPostFromImport(array $data = [])
	{
		$search = htmlspecialchars_decode($data['detail_text']).' '.htmlspecialchars_decode($data['preview_text']);
		$search = strip_tags($search);
		$search = mb_strtoupper($search);
		$search = str_replace([
			chr(13), // новая строка
			chr(10), // перевод каретки
			chr(9), // табуляция
//			chr(32), // пробелы
		], '', $search);
		$data['searchable_content'] = $search;

		$this->fill($data);

		$result = $this->save();
		if ($result){
			static::saveTags($this);
		}

		return $this->id;
	}

	public function updatePostFromImport(Post $item, array $data = [])
	{
		$search = htmlspecialchars_decode($data['detail_text']).' '.htmlspecialchars_decode($data['preview_text']);
		$search = strip_tags($search);
		$search = mb_strtoupper($search);
		$search = str_replace([
			chr(13), // новая строка
			chr(10), // перевод каретки
			chr(9), // табуляция
//			chr(32), // пробелы
		], '', $search);
		$data['searchable_content'] = $search;

		$item->fill($data);
		$result = $item->save();

		if ($result){
			static::saveTags($item);
		}

		return $item->id;
	}

	public static function saveTags(Post $post)
	{
		$obTag = new Tags();
		$arTags = explode(',', $post->tags);
		foreach ($arTags as $tag) {
			$tag = trim($tag);
			$currentTag = $obTag::wherePostId($post->id)->whereTag($tag)->first();
			if (is_null($currentTag)){
				$obTag->fill([
					'post_id' => $post->id,
					'tag' => $tag,
				])->save();
			} else {
				$currentTag->fill([
					'post_id' => $post->id,
					'tag' => $tag,
				])->save();
			}
		}
	}

	public static function saveSeoData(Post $post, array $seoProps)
	{
		$dataSeo = new SeoData();
		$currentSeo = SeoData::whereElementId($post->id)->whereType('blog_post')->first();
		if (is_null($currentSeo)){
			$dataSeo->fill([
				'title' => $seoProps['ELEMENT_META_TITLE'],
				'description' => $seoProps['ELEMENT_META_DESCRIPTION'],
				'keyword' => $seoProps['ELEMENT_META_KEYWORDS'],
				'page_title' => $seoProps['ELEMENT_PAGE_TITLE'],
				'type' => 'blog_post',
				'element_id' => $post->id,
			])->save();
		} else {
			$currentSeo->fill([
				'title' => $seoProps['ELEMENT_META_TITLE'],
				'description' => $seoProps['ELEMENT_META_DESCRIPTION'],
				'keyword' => $seoProps['ELEMENT_META_KEYWORDS'],
				'page_title' => $seoProps['ELEMENT_PAGE_TITLE'],
			])->save();
		}
	}

	public static function result(Post $post)
	{

	}

	/**
	 * @method getTagsAttribute
	 * @param $val
	 *
	 * @return \Illuminate\Support\Collection|string
	 */
	public function getTagsAttribute($val)
	{
		$val = trim($val);
		if (is_string($val) && strlen($val) > 0){
			$arTags = collect(explode(',', $val))->map(function ($el) {
				$el = trim($el);

				return [
					'tag' => $el,
					'url' => '/blog/tag/'.Transliteration::translit($el),
				];
			});

			return $arTags;
		}

		return $val;
	}

	/**
	 * @method previewPicture
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne|Files
	 */
	public function previewPicture()
	{
		return $this->hasOne(Files::class, 'id', 'preview_picture')->withDefault();
	}

	public function sectionCode()
	{
		return $this->hasOne(Sections::class, 'id', 'section_id')
			->select(['id','name','code'])->withDefault();
	}


	/**
	 * @method getSmallImage
	 * @return mixed
	 */
	public function getSmallImage()
	{
		return $this->previewPicture->resizeImage('preview', ['width' => 800, 'height' => 460]);
	}

	public function getRouteKeyName()
	{
		return 'postDetail';
	}

	/**
	 * @method popularPosts
	 * @return Post[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
	 */
	public static function popularPosts()
	{
		$posts = Post::with('sectionCode')->orderByDesc('id')->limit(3)->select([
			'id', 'name', 'code', 'active_from', 'preview_picture', 'section_id'
		])->get();
		$imagesId = $posts->map(function (Post $el) {
			return $el->preview_picture;
		});
		$imgCollection = Files::whereIn('id', $imagesId->all())->select(['id', 'name', 'sub_dir'])->get()->map(function (Files $files) {
			$files->setResizedImg($files->resizeImage('mini', ['width' => 64, 'height' => 64]));

			return $files;
		});

		$resultPost = $posts->map(function (Post $post) use ($imgCollection){
			$img = $imgCollection->first(function (Files $el) use ($post){
				return $el->id === $post->preview_picture;
			});
			$post->preview_picture = $img;

			return $post;
		});

		unset($posts, $imgCollection, $imagesId);

		return $resultPost;
	}

	/**
	 * @method detailPicture
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne|Files
	 */
	public function detailPicture()
	{
		return $this->hasOne(Files::class, 'id', 'detail_picture')->withDefault();
	}

	/**
	 * @method property
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne|PropertyPost
	 */
	public function property()
	{
		return $this->hasOne(PropertyPost::class, 'post_id', 'id');
	}
}
