<?php

namespace App\Entity\Blog;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Blog\PropertyPostMulti
 *
 * @property int $post_id
 * @property string|null $value
 * @property string|null $description
 * @property int|null $value_num
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPostMulti whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPostMulti wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPostMulti whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPostMulti whereValueNum($value)
 * @mixin \Eloquent
 * @property int $id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPostMulti whereId($value)
 */
class PropertyPostMulti extends Model
{
	protected $table = 'dar_blog_post_property_multi';
	public $timestamps = false;
//	protected $primaryKey = 'post_id';
//	public $incrementing = false;
	protected $fillable = [
		'post_id', 'value', 'description', 'value_num'
	];
}
