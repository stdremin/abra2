<?php

namespace App\Entity\Blog;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Blog\PropertyPost
 *
 * @property int $post_id
 * @property string $downloads
 * @property string|null $source_link
 * @property string|null $source_title
 * @property string $links_post
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPost whereDownloads($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPost whereLinksPost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPost wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPost whereSourceLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\PropertyPost whereSourceTitle($value)
 * @mixin \Eloquent
 */
class PropertyPost extends Model
{
	protected $table = 'dar_blog_post_property';
	public $timestamps = false;
	protected $primaryKey = 'post_id';
	public $incrementing = false;

	/** @var \Illuminate\Database\Eloquent\Collection */
	protected $linkItems = null;

	protected $fillable = [
		'post_id', 'downloads', 'source_link', 'source_title', 'links_post',
	];

	/**
	 * @method saveProperties
	 * @param Post $post
	 * @param array $props
	 */
	public static function saveProperties(Post $post, array $props)
	{
		$props['post_id'] = $post->id;
		$multiProp = [];
		if (array_key_exists('links_post', $props)){
			$multiProp = $props['links_post'];
		}
		$postItems = collect([]);
		if (count($multiProp) > 0){
			$postIdsCollection = Post::select(['id', 'name', 'code'])->whereIn('xml_id', $multiProp)->get();

			/** @var Post $item */
			foreach ($postIdsCollection as $item) {
				$postItems->offsetSet($item->id, $item->getAttributes());
			}
			$props['links_post'] = serialize($postItems->all());
		}

		$id = null;
		$property = static::wherePostId($post->id)->first();
		if (!is_null($property)){
			$property->fill($props)->save();
			$id = $property->post_id;
		} else {
			$id = static::create($props)->post_id;
		}

		if ((int)$id > 0 && $postItems->count() > 0){
			foreach ($postItems as $k => $value) {
				$multiItem = PropertyPostMulti::wherePostId($post->id)->whereValueNum($k)->first();
				$saveMulti = [
					'post_id' => $post->id,
					'value' => $value['name'],
					'value_num' => $k,
				];
				if (is_null($multiItem)){
					PropertyPostMulti::create($saveMulti);
				} else {
					$multiItem->fill($saveMulti)->save();
				}
			}
		}
	}

	public static function getPropertyPost(Post $post, $select = ['*'])
	{
		return static::wherePostId($post->id)->first();
	}

	public function getLinksPostAttribute($arLinks)
	{
		$arLinks = unserialize($arLinks);

		if (is_null($this->linkItems)){
			$this->linkItems = Post::whereIn('id', array_keys($arLinks))->with('sectionCode')
				->select(['id', 'name', 'code', 'section_id', 'preview_picture', 'preview_text'])
				->get();
		}

		return $this->linkItems;
	}

	/**
	 * @method getLinkItems - get param linkItems
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function getLinkItems()
	{
		return $this->linkItems;
	}


}
