<?php

namespace App\Entity\Blog;

use App\Helpers\Transliteration;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Entity\Blog\Tags
 *
 * @property int $id
 * @property int $post_id
 * @property string $tag
 * @property int $counter
 * @property int|null $sort
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Tags whereCounter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Tags whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Tags wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Tags whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Tags whereTag($value)
 * @mixin \Eloquent
 * @property string|null $code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Blog\Tags whereCode($value)
 */
class Tags extends Model
{
	protected $table = 'dar_blog_tags';
	public $timestamps = false;
	protected $fillable = ['post_id', 'tag', 'counter', 'sort', 'code'];

	/**
	 * @method saveTag
	 * @param int $postId
	 * @param $tags
	 */
	public function saveTag(int $postId, $tags)
	{
		$arTag = explode(',', $tags);
		$items = $this->wherePostId($postId)->whereIn('tag', $arTag);
		if ($items->count() == 0){
			foreach ($arTag as $item) {
				$name = trim($item);
				$res = static::create([
					'post_id' => $postId,
					'tag' => $name,
					'code' => Transliteration::translit($name)
				]);
			}
		}
	}

	/**
	 * @method cloudTag
	 * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	public static function cloudTag()
	{
		$items = Tags::query()->select(['*', DB::raw("COUNT('tag') as cnt")])
			->groupBy('tag')
			->orderBy('cnt', 'DESC')
			->limit(15)->get();

		return $items;
	}
}
