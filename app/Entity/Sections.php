<?php

namespace App\Entity;

use App\Entity\Blog\Post;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\Sections
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_create
 * @property int $user_update
 * @property int $id_block
 * @property int|null $parent_id
 * @property int $active
 * @property int|null $sort
 * @property string $name
 * @property int|null $picture
 * @property int|null $left_margin
 * @property int|null $right_margin
 * @property int $level
 * @property string|null $description
 * @property string|null $searchable_content
 * @property string|null $code
 * @property string|null $xml_id
 * @property int|null $detail_picture
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereDetailPicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereIdBlock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereLeftMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereRightMargin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereSearchableContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereUserCreate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereUserUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Sections whereXmlId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\Blog\Post[] $posts
 */
class Sections extends Model
{

	protected $table = 'dar_sections';
	protected $fillable = [
		'user_create', 'user_update', 'id_block', 'parent_id', 'sort', 'active',
		'name', 'picture', 'left_margin', 'right_margin', 'level', 'description',
		'searchable_content', 'code', 'xml_id', 'detail_picture',
	];

	/**
	 * @method getByXmlId
	 * @param string|integer $xmlId
	 *
	 * @return Sections[]|\Illuminate\Database\Eloquent\Collection
	 */
	public static function getByXmlId($xmlId)
	{
		return static::select(['*'])
			->where('xml_id', '=', trim($xmlId))
			->first();
	}

	/**
	 * @method section
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany|Post
	 */
	public function posts()
	{
		return $this->hasMany(Post::class, 'id', 'section_id');
	}

}
