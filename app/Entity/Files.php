<?php

namespace App\Entity;

use App\Helpers\Tools;
use Illuminate\Contracts\Filesystem\FileExistsException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * App\Entity\Files
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $height
 * @property int|null $width
 * @property int $size
 * @property string $size_format
 * @property string $content_type
 * @property string|null $sub_dir
 * @property string $name
 * @property string|null $original_name
 * @property string $hash
 * @property string|null $description
 * @property string|null $xml_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereSizeFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereSubDir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Files whereXmlId($value)
 */
class Files extends Model
{
	protected $table = 'dar_files';

	public static $folder = 'app/public';

	protected $resizedImg;

	protected $fillable = [
		'height', 'width', 'sub_dir', 'name', 'original_name', 'description', 'xml_id', 'path',
		'content_type', 'size_format', 'hash', 'size',
	];

	/**
	 * @method saveFile
	 * @param array $options
	 *
	 * @return Files|bool|Model
	 * @throws FileExistsException
	 */
	public static function saveFile(array $options = [])
	{
		$path = $options['sub_dir'].'/'.$options['name'];
		$disk = Storage::disk('public');
		if (!$disk->exists($path)){
			$msg = sprintf('file %s [%s] not found', $options['name'], $path);
			throw new FileExistsException($msg);
		}


		$options['hash'] = File::hash($disk->path($path));
		$options['size'] = (int)$disk->size($path);
		$options['size_format'] = Tools::formatSize($options['size']);
		$options['content_type'] = $disk->mimeType($path);

//		$options['xml_id'] = $options['xml_id'] ? : $options['hash'];

		$item = static::whereName(trim($options['name']))->first();

		if ($item){
//			dd($options);
			$item->update($options);
			$result = $item->id;
		} else {
			$result = static::create($options)->id;
		}

		return $result;
	}

	/**
	 * @method resizeImage
	 * @param string $name
	 * @param array $options
	 *
	 * @return string
	 */
	public function resizeImage(string $name, array $options = ['default' => false])
	{
		if (!is_null($this->id)){
			$path = storage_path(self::$folder.'/'.$this->sub_dir.'/'.$this->name);
			$resizeName = $name.'_'.$this->id.'_'.$options['width'].'_'.$options['height'].'.'.File::extension($path);
			$arResizeImg = [
				'app/public',
				$this->sub_dir,
				'resize',
			];
			$folder = storage_path(implode('/', $arResizeImg));
			$resizeImg = $folder.'/'.$resizeName;

			if(!File::exists($folder)){
				File::makeDirectory($folder, 0755,true);
			}

			if(!File::exists($resizeImg)){
				$resizer = Image::make($path);
				$resizer->resize($options['width'], $options['height']);
				$resizer->save($resizeImg, 80);
			}

			$localPath = Storage::url(Tools::localStoragePath($resizeImg));

			return asset($localPath);
		}

		return $options['default'] ?: asset('assets/images/post-5.jpg');
	}

	/**
	 * @method getResizedImg - get param resizedImg
	 * @return mixed
	 */
	public function getResizedImg()
	{
		return $this->resizedImg;
	}

	/**
	 * @param mixed $resizedImg
	 *
	 * @return Files
	 */
	public function setResizedImg($resizedImg)
	{
		$this->resizedImg = $resizedImg;

		return $this;
	}

	/**
	 * @method getOriginalPath
	 * @return string
	 */
	public function getOriginalPath()
	{
		$path = $this->sub_dir.'/'.$this->name;
		$localPath = Storage::url(Tools::localStoragePath($path));

		return asset($localPath);
	}
}
