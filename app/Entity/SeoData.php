<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entity\SeoData
 *
 * @property int $id
 * @property int $element_id
 * @property string $type
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keyword
 * @property string|null $page_title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SeoData whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SeoData whereElementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SeoData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SeoData whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SeoData wherePageTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SeoData whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\SeoData whereType($value)
 * @mixin \Eloquent
 */
class SeoData extends Model
{
	protected $table = 'dar_seo_data';
	public $timestamps = false;

	protected $fillable = [
		'element_id', 'type', 'title', 'description', 'keyword', 'page_title',
	];
}
