<?php

namespace App\Console\Commands;

use App\Http\Controllers\Blog\Import;
use App\Log\FilesLog;
use Illuminate\Console\Command;

class ImportBlog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:blog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import blog from abra';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$_SERVER['DOCUMENT_ROOT'] = __DIR__.'/../../../public';
    	$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$Import = new Import();
    	try {
		    $Import->importPostFormXml(new FilesLog());
	    } catch (\Exception $exception){
    		dump($exception);
	    }

    }
}
