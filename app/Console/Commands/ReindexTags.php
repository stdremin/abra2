<?php

namespace App\Console\Commands;

use App\Entity\Blog\Post;
use App\Entity\Blog\Tags;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ReindexTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blog:reindex_tag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex tags';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
	    $_SERVER['DOCUMENT_ROOT'] = __DIR__.'/../../../public';
	    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	DB::select('TRUNCATE TABLE dar_blog_tags');

    	$postItems = Post::all();
    	$tag = new Tags();
    	/** @var Post $postItem */
	    foreach ($postItems as $postItem) {
		    $tag->saveTag($postItem->id, $postItem->getOriginal('tags'));
    	}
    }
}
