<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ImportBackup extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'backup:unpack {file}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Распаковка бекапа';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		if (function_exists('date_default_timezone_set'))
			date_default_timezone_set('Europe/Moscow');

		$_SERVER['DOCUMENT_ROOT'] = __DIR__.'/../../../public';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

		parent::__construct();
	}

	public function handle()
	{
		$file = storage_path('backup/'.$this->argument('file'));
		$ext = File::extension($file);
		if (strtolower($ext) == 'zip'){
			$zip = new \ZipArchive();
			$zip->open($file);
			$zip->extractTo(storage_path('backup'));
			$zip->close();
		}
		$sql = preg_replace('/zip$/i', 'sql', $file);

		if (!File::exists($sql) || File::extension($sql) !== 'sql'){
			throw new \Exception('File sql not found');
		}

		$config = config('database.connections.mysql');
		$command = sprintf(
			"mysql --user=%s --password=%s --host=%s %s < %s",
			$config["username"], $config["password"], $config["host"],
			$config["database"], $sql
		);

		exec($command);
	}
}
