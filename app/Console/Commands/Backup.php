<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Backup extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'backup:dump';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Создание архива приложения вместе с msql';

	/** @var string */
	protected $fileDump;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		if (function_exists('date_default_timezone_set'))
			date_default_timezone_set('Europe/Moscow');

		$_SERVER['DOCUMENT_ROOT'] = __DIR__.'/../../../public';
		$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->clearCache();
		$this->dumpSql();
//		$this->dumpFiles();
	}

	protected function dumpSql()
	{
		$dumpFile = $this->fileDump();
		$config = config('database.connections.mysql');
		$command = sprintf(
			"mysqldump --user=%s --password=%s --host=%s %s > %s",
			$config["username"], $config["password"], $config["host"],
			$config["database"], $dumpFile
		);

		exec($command);

		$fileZipSql = $sql = preg_replace('/sql$/i', 'zip', $dumpFile);
		$zip = new \ZipArchive();
		$zip->open($fileZipSql, \ZipArchive::CREATE);
		$zip->addFile($dumpFile);
		$zip->close();
		File::delete($dumpFile);

		return $this;
	}

	protected function dumpFiles()
	{
		if (File::exists(storage_path('/logs/laravel.log'))){
			File::delete(storage_path('/logs/laravel.log'));
		}

		$fileZip = base_path().'/'.date('d_m_Y_His').'.tar.gz';
		exec('tar -czvf '.$fileZip.'.tar.gz'.' '.storage_path());

		return $this;
	}

	protected function clearCache()
	{
		$this->callSilent('view:cache');
		$this->callSilent('view:clear');
		$this->callSilent('cache:clear');
		$this->callSilent('debugbar:clear');

		return $this;
	}

	protected function fileDump()
	{
		$folder = storage_path('backup');
		if (!File::exists($folder)){
			File::makeDirectory($folder, 0755, true);
		}
		if (strlen($this->fileDump) == 0){
			$file = $folder.'/'.date('d_m_Y_His').'.sql';
			if (!File::exists($file)){
				File::put($file, "--- START \n");
			}

			$this->fileDump = $file;
		}

		return $this->fileDump;
	}
}
