<?php

namespace App\Providers;

use App\Helpers\Tools;
use App\Log\FilesLog;
use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    	$this->app->singleton('blog.tools', Tools::class);
		$this->app->bind('blog.log', new FilesLog());
    }
}
