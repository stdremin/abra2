<?php

namespace App\Http\Controllers\Blog;

use App\Entity\Blog\Post;
use App\Entity\Blog\PropertyPost;
use App\Entity\Blog\Tags;
use App\Entity\Sections;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class Main extends Controller
{

	public function index(Sections $sections, Post $post)
	{
		$data = $post::query()->with('sectionCode')->select(['*'])
			->where('active_from', '<=', new \DateTime())
			->orWhere('active_from', '=', '')
			->orderByDesc('id')
			->paginate(20);

		$blogSections = $sections::all();


		return view('blog.items', [
			'data' => $data,
			'sections' => $blogSections,
			'populars' => Post::popularPosts(),
			'cloudTags' => Tags::cloudTag(),
		]);
	}

	/**
	 * @method sections
	 * @param $code
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function sections($code)
	{
		$sections = \App\Entity\Sections::select(['id', 'name', 'code'])->get();
		$data = Post::query()->join('dar_sections as DS', 'DS.id', '=', 'dar_blog_posts.section_id')
			->where('DS.code', '=', $code)
			->orderByDesc('dar_blog_posts.active_from')
			->select([
				'dar_blog_posts.id', 'dar_blog_posts.name', 'dar_blog_posts.code', 'dar_blog_posts.preview_picture',
				'dar_blog_posts.tags', 'dar_blog_posts.sort', 'dar_blog_posts.active_from',
				'dar_blog_posts.section_id', 'dar_blog_posts.preview_text',
				'DS.code as section_code', 'DS.name as section_name',
			])
			->paginate(20);

		$current = $sections->filter(function ($el) use ($code) {
			return $code === $el['code'];
		})->first();

		return view('blog.items', [
			'data' => $data,
			'sections' => $sections,
			'current' => $current,
			'populars' => Post::popularPosts(),
			'cloudTags' => Tags::cloudTag(),
		]);
	}

	public function tag($code)
	{
		$postIds = Tags::whereCode($code)
			->groupBy('post_id')->get()
			->map(function (Tags $el) {
				return $el->post_id;
			});

		$elements = Post::with('sectionCode')->whereIn('id', $postIds->all())
			->where('active_from', '<=', new \DateTime())
			->orWhere('active_from', '=', '')
			->select([
				'id', 'name', 'code', 'preview_text', 'preview_picture',
				'section_id', 'active_from', 'sort', 'tags'
			])->paginate(15);

		$sections = \App\Entity\Sections::select(['id', 'name', 'code'])->get();

		return view('blog.items', [
			'data' => $elements,
			'sections' => $sections,
			'current' => [],
			'populars' => Post::popularPosts(),
			'cloudTags' => Tags::cloudTag(),
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param string $sectionCode
	 * @param string $code
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($sectionCode, $code)
	{
		$section = Sections::whereCode($sectionCode)->first(['id']);
		$post = Post::with('sectionCode')->whereCode($code)->whereSectionId($section->id)->first();
		$props = PropertyPost::getPropertyPost($post);

		return view('blog.detail', [
			'data' => $post,
			'sections' => Sections::all(),
			'populars' => Post::popularPosts(),
			'props' => $props,
			'cloudTags' => Tags::cloudTag(),
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Post $post)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Post $post)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Post $post
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Post $post)
	{
		//
	}
}
