<?php
/**
 * Created by OOO 1C-SOFT.
 * User: GrandMaster
 * Date: 05.10.18
 */

namespace App\Http\Controllers\Blog;


use App\Entity\Blog\Post;
use App\Entity\Blog\PropertyPost;
use App\Entity\Files;
use App\Entity\Sections;
use App\Entity\SeoData;
use App\Helpers\Tools;
use App\Helpers\Transliteration;
use App\Http\Controllers\Controller;
use App\Log\FilesLog;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use GuzzleHttp\Client;

class Import extends Controller
{

	/** @var FilesLog */
	protected $filesLog;
	protected $imageDownload = false;
	protected $loadFromFiles = false;

	public function importPostFormXml(FilesLog $filesLog)
	{
//		throw new \Exception('IMPORT IS END');

		$this->filesLog = $filesLog;
		$this->filesLog->deleteFile();

//		$xmlSections = $this->fetchSections();
//		$this->addSections($xmlSections);
		$this->addPostBlog($this->fetchPostBlog());
	}

	protected function fetchSections()
	{
		if ($this->loadFromFiles){
			$file = Tools::root().'/upload/sections.xml';

			return File::get($file);
		} else {
			$client = new Client([
				'base_url' => 'https://abraxabra.ru/export_new_blog',
				'auth' => ['st.dremin', 'Evergreen;-)12'],
			]);
			$response = $client->request('GET', 'https://abraxabra.ru/export_new_blog/sections.php');

			return $response->getBody()->getContents();
		}
	}

	protected function fetchPostBlog()
	{
		if ($this->loadFromFiles){
			$file = Tools::root().'/upload/posts.xml';

			return File::get($file);
		} else {
			$client = new Client([
				'auth' => ['st.dremin', 'Evergreen;-)12'],
			]);
			$response = $client->request('GET', 'https://abraxabra.ru/export_new_blog');

			return $response->getBody()->getContents();
		}
	}

	protected function addSections($xml)
	{
		$obSection = new Sections();
		$data = (new XmlEncoder())->decode($xml, 'xml');
		foreach ($data as $section) {
			$imgId = 0;
//			$this->filesLog->log($section);

			if (!empty($section['PICTURE']) && $this->imageDownload){
				$fileName = 'blog/sections/'.$section['PICTURE']['FILE_NAME'];
				if (Storage::put($fileName, file_get_contents('https://abraxabra.ru'.$section['PICTURE']['SRC']))){
					$imgId = Files::saveFile([
						'sub_dir' => 'blog/sections',
						'original_name' => $section['PICTURE']['ORIGINAL_NAME'],
						'name' => $section['PICTURE']['FILE_NAME'],
					]);
				}
			}

			$currentSection = $obSection->whereXmlId($section['ID'])->first();
			$save = [
				'user_create' => 1,
				'user_update' => 1,
				'id_block' => 1,
				'name' => $section['NAME'],
				'code' => $section['CODE'],
				'sort' => $section['SORT'],
				'xml_id' => $section['ID'],
				'description' => $section['DESCRIPTION'],
				'left_margin' => $section['LEFT_MARGIN'],
				'right_margin' => $section['RIGHT_MARGIN'],
			];

			if ($this->imageDownload && (int)$imgId > 0){
				$save['picture'] = $imgId;
			}
			$id = 0;
			if (!is_null($currentSection)){
				$currentSection->fill($save);
				$res = $currentSection->save();
				if ($res)
					$id = $currentSection->id;
			} else {
				$res = $obSection->fill($save)->save();
				if ($res){
					$id = $obSection->id;
				}
			}

			if ($id > 0){
				$seoData = SeoData::whereElementId($id)->whereType('section_blog')->first();
				if (!is_null($seoData)){
					$seoData
						->fill([
							'title' => $section['SEO_PROPERTY']['SECTION_META_TITLE'],
							'description' => $section['SEO_PROPERTY']['SECTION_META_DESCRIPTION'],
							'keyword' => $section['SEO_PROPERTY']['ELEMENT_META_KEYWORDS'],
							'page_title' => $section['SEO_PROPERTY']['ELEMENT_META_TITLE'],
						])->save();
				} else {
					$seoData = new SeoData();
					$seoData
						->fill([
							'title' => $section['SEO_PROPERTY']['SECTION_META_TITLE'],
							'description' => $section['SEO_PROPERTY']['SECTION_META_DESCRIPTION'],
							'keyword' => $section['SEO_PROPERTY']['ELEMENT_META_KEYWORDS'],
							'page_title' => $section['SEO_PROPERTY']['ELEMENT_META_TITLE'],
							'type' => 'section_blog',
							'element_id' => $id,
						])->save();
				}
			}
		}
	}

	protected function addPostBlog($xml)
	{

		$data = (new XmlEncoder())->decode($xml, 'xml');

//		$this->filesLog->log($data);

		foreach ($data as $k => $item) {
//			if($k > 5) break;
			$obPost = new Post();

			$currentElement = $obPost->whereXmlId($item['ID'])->first();
			$section = Sections::whereXmlId($item['IBLOCK_SECTION_ID'])->first();
			$imgId = $prevImgId = null;
//			$this->filesLog->log($section);

			$re = '/<pre.*><code.*>(.*)<\/code><\/pre>/sU';

			if(!preg_match_all($re, $item['DETAIL_TEXT'], $detailText, PREG_SET_ORDER, 0)){
				$re = '/<pre.*>(.*)<\/pre>/sU';
				preg_match_all($re, $item['DETAIL_TEXT'], $detailText, PREG_SET_ORDER, 0);
			}

			foreach ($detailText as $v) {
				if(array_key_exists(1, $v)){
					$item['DETAIL_TEXT'] = str_replace($v[1], htmlspecialchars($v[1]), $item['DETAIL_TEXT']);
				}
			}

			if ($this->imageDownload){
				if (!empty($item['PREVIEW_PICTURE'])){
					$filePreview = 'blog/post/'.$item['PREVIEW_PICTURE']['FILE_NAME'];
					if (Storage::put($filePreview, file_get_contents('https://abraxabra.ru'.$item['PREVIEW_PICTURE']['SRC']))){
						$prevImgId = Files::saveFile([
							'sub_dir' => 'blog/post',
							'original_name' => $item['PREVIEW_PICTURE']['ORIGINAL_NAME'],
							'name' => $item['PREVIEW_PICTURE']['FILE_NAME'],
						]);
					}
				}

				if (!empty($item['DETAIL_PICTURE'])){
					$fileDetail = 'blog/post/'.$item['DETAIL_PICTURE']['FILE_NAME'];
					if (Storage::put($fileDetail, file_get_contents('https://abraxabra.ru'.$item['DETAIL_PICTURE']['SRC']))){
						$imgId = Files::saveFile([
							'sub_dir' => 'blog/post',
							'original_name' => $item['DETAIL_PICTURE']['ORIGINAL_NAME'],
							'name' => $item['DETAIL_PICTURE']['FILE_NAME'],
						]);
					}
				}
			}

			$save = [
				'user_create' => 1,
				'user_update' => 1,
				'id_block' => 1,
				'section_id' => $section->id,
				'sort' => (int)$item['SORT'],
				'name' => $item['NAME'],
				'preview_text' => $item['PREVIEW_TEXT'],
				'detail_text' => $item['DETAIL_TEXT'],
				'status' => Post::STATUS_PUBLISHING,
				'xml_id' => $item['ID'],
				'code' => $item['CODE'],
				'tags' => $item['TAGS'],
			];
			if (strlen($item['ACTIVE_FROM']) > 0){
				$save['active_from'] = new \DateTime($item['ACTIVE_FROM']);
			}
			if (strlen($item['ACTIVE_TO']) > 0){
				$save['active_to'] = new \DateTime($item['ACTIVE_TO']);
			}

			if ($this->imageDownload && (int)$imgId > 0){
				$save['detail_picture'] = $imgId;
			}
			if ($this->imageDownload && (int)$prevImgId > 0){
				$save['preview_picture'] = $prevImgId;
			}

			$arProps = collect($item['PROPERTY']['ITEMS']);

			$multi = $this->findProp($arProps, 'ADD_POST')['VALUE'];

			$props = [
				'downloads' => $this->findProp($arProps, 'ATT_DOP_FILE')['VALUE'],
				'source_link' => $this->findProp($arProps, 'ATT_URL_ISTOCNIK')['VALUE'],
				'source_title' => $this->findProp($arProps, 'ATT_NAME_ISTOCNIK')['VALUE'],
			];

			if ($multi && array_key_exists('ITEM', $multi)){
				$props['links_post'] = $multi['ITEM'];
			}


			if (!is_null($currentElement)){
				$obPost->updatePostFromImport($currentElement, $save);
				Post::saveSeoData($currentElement, $item['SEO_PROPERTY']);
				PropertyPost::saveProperties($currentElement, $props);
			} else {
				$obPost->addPostFromImport($save);
				Post::saveSeoData($obPost, $item['SEO_PROPERTY']);
				PropertyPost::saveProperties($obPost, $props);
			}
		}
	}

	private function findProp(Collection $items, $code)
	{
		return $items->first(function ($el) use ($code) {
			return $el['CODE'] == $code;
		});
	}
}
