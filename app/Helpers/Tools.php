<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 05.10.2018
 */

namespace App\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\VarDumper;
use TarunMangukiya\ImageResizer\ImageResizer;

class Tools
{

	/**
	 * @method root
	 * @return string
	 */
	public static function root(): string
	{
		return Request::server('DOCUMENT_ROOT') ?: $_SERVER['DOCUMENT_ROOT'];
	}

	/**
	 * @method localPath
	 * @param string $fileName
	 *
	 * @return string
	 */
	public static function localPath(string $fileName): string
	{
		return str_replace(self::root(), '', $fileName);
	}

	public static function localStoragePath(string $fileName): string
	{
		$str = str_replace(storage_path().'/app/public', '', $fileName);

		return ltrim($str, '/');
	}

	public static function storageUrl(string $file)
	{
		return str_replace('//', '', $file);
	}

	public static function copyImgPostBlog($path = '', $subDir = '')
	{
		if (strlen($path) > 0){
			$from = static::root().'/upload/import/'.$path;
			$to = static::root().'/upload/blog'.$subDir.'/'.File::basename($from);

			if (!File::exists($to)){
				File::delete($to);
			}
			if (!File::exists(static::root().'/upload/blog'.$subDir)){
				File::makeDirectory(static::root().'/upload/blog'.$subDir);
			}

			return File::copy($from, $to) ? $to : false;
		}

		return false;
	}

	/**
	 * @method formatSize
	 * @param int $size
	 * @param int $precision
	 *
	 * @return string
	 */
	public static function formatSize($size = 0, $precision = 2)
	{
		static $a = array("b", "Kb", "Mb", "Gb", "Tb");
		$pos = 0;
		while($size >= 1024 && $pos < 4)
		{
			$size /= 1024;
			$pos++;
		}
		return round($size, $precision)." ".$a[$pos];
	}

	/**
	 * @method varDumperHandler
	 */
	public static function varDumperHandler(): void
	{
		VarDumper::setHandler(function ($var) {
			$cloner = new VarCloner();
			if (in_array(PHP_SAPI, array('cli', 'phpdbg'), true)){
				$dumper = new CliDumper();
				$dumper->dump($cloner->cloneVar($var));
			} else {
				$iteArgs = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
				$iteArgs = collect($iteArgs);

				$bt = $iteArgs->first(function ($el) {
					if(!array_key_exists('class', $el)){
						return ($el['function'] == 'dump' || $el['function'] == 'dd');
					}

//					if ($el['class'] !== Symfony\Component\VarDumper\VarDumper::class){
//						return ($el['function'] == 'dump' || $el['function'] == 'dd');
//					}

					return false;
				});

				$dumper = new HtmlDumper();
				$dumper->setStyles([
					'default' => 'outline:none; line-height:1.2em; font:14px Menlo, Monaco, Consolas, monospace; word-wrap: break-word; position:relative; z-index:99999; word-break: normal; margin-bottom: 0',
					'key' => 'color: #79016f',
					'num' => 'font-weight:bold; color:#1299DA',
					'const' => 'font-weight:bold',
					'str' => 'font-weight:bold; color:#000',
					'note' => 'color:#1299DA',
					'ref' => 'color:#A0A0A0',
					'public' => 'color:#0f9600',
					'protected' => 'color:#a300bf',
					'private' => 'color:#ec0000',
					'meta' => 'color:#B729D9',
					'index' => 'color:#1299DA',
					'ellipsis' => 'color:#FF8400',
				]);

				echo '<div style="font-size:9pt; color:#000; background:#fff; border:1px dashed #000; z-index: 700; position: relative">
							<div style="padding:3px 5px; background:#99CCFF; font-weight:bold;">File: '.$bt["file"].'
								['.$bt["line"].']
							</div>
							<div style="padding:10px; background: #fff; text-align: left">';
				echo $dumper->dump($cloner->cloneVar($var));
				echo '</div>
						</div>';

			}
		});
	}

	/**
	 * Returns array backtrace.
	 *
	 * @param integer $limit Maximum stack elements to return.
	 * @param null|integer $options Passed to debug_backtrace options.
	 * @param integer $skip How many stack frames to skip.
	 *
	 * @return array
	 * @see debug_backtrace
	 */
	public static function getBackTrace($limit = 0, $options = null, $skip = 1)
	{
		if(!defined("DEBUG_BACKTRACE_PROVIDE_OBJECT"))
		{
			define("DEBUG_BACKTRACE_PROVIDE_OBJECT", 1);
		}

		if ($options === null)
		{
			$options = ~DEBUG_BACKTRACE_PROVIDE_OBJECT;
		}

		if (PHP_VERSION_ID < 50306)
		{
			$trace = debug_backtrace($options & DEBUG_BACKTRACE_PROVIDE_OBJECT);
		}
		elseif (PHP_VERSION_ID < 50400)
		{
			$trace = debug_backtrace($options);
		}
		else
		{
			$trace = debug_backtrace($options, ($limit > 0? $limit + 1: 0));
		}

		if ($limit > 0)
		{
			return array_slice($trace, $skip, $limit);
		}

		return array_slice($trace, $skip);
	}

}