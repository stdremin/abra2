<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'blog', 'namespace' => 'Blog'], function (){
	Route::get('/', 'Main@index')->name('blog.index');
	Route::get('/{code}', 'Main@sections')->name('blog.section');
	Route::get('/detail', function (){
		return redirect('/blog');
	});
	Route::get('/tag/{tag}', 'Main@tag')->name('section.tag');
	Route::get('/{section_code}/{code}', 'Main@show')->name('post.detail');

});

Route::get('/import', 'Blog\Import@importPostFormXml');

