<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Entity\Blog\Post;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_blog_posts', function (Blueprint $table) {
	        $table->engine = "InnoDB";

            $table->increments('id');
            $table->timestamps();
            $table->integer('user_create')->nullable(false);
            $table->integer('user_update')->nullable(false);
            $table->integer('section_id')->nullable();
            $table->integer('id_block')->nullable(false);
            $table->boolean('active')->default(true);
            $table->dateTime('active_from');
            $table->dateTime('active_to');
            $table->integer('sort')->default(500);
            $table->string('name', 255)->nullable(false);
            $table->integer('preview_picture')->nullable();
            $table->integer('detail_picture')->nullable();
            $table->text('preview_text');
            $table->longText('detail_text');
            $table->longText('searchable_content');
            $table->enum('status', [Post::STATUS_DRAFT, Post::STATUS_PUBLISHING])->default(Post::STATUS_DRAFT);
			$table->uuid('xml_id')->nullable();
			$table->string('code', 255)->nullable();
			$table->string('tags', 255)->nullable();
			$table->integer('counter_view')->default(0);

			$table->index(['section_id'], 'ix_b_post_section_id');
			$table->index('code', 'ix_b_post_code');
			$table->index('xml_id', 'ix_b_post_xml_id');
//			$table->foreign('section_id', 'fk_post_section_id')->on('dar_sections')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_blog_posts');
    }
}
