<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_blog_tags', function (Blueprint $table) {
	        $table->engine = "InnoDB";
            $table->increments('id');
			$table->integer('post_id')->nullable(false);
			$table->string('tag', 255)->nullable(false);
			$table->integer('counter')->default(0);
			$table->integer('sort')->nullable();

			$table->index('post_id', 'ix_tags_post_id');
			$table->index('tag', 'ix_tags_tag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_blog_tags');
    }
}
