<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_seo_data', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('element_id')->nullable(false)->index();
			$table->string('type')->default('blog_post');
			$table->string('title', 255)->nullable();
			$table->string('description', 255)->nullable();
			$table->string('keyword', 255)->nullable();
			$table->string('page_title', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_seo_data');
    }
}
