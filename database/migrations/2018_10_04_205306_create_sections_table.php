<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_sections', function (Blueprint $table) {
	        $table->engine = "InnoDB";
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_create')->nullable(false);
            $table->integer('user_update')->nullable(false);
            $table->integer('id_block')->nullable(false);
            $table->integer('parent_id')->nullable();
            $table->boolean('active')->default(true);
            $table->integer('sort')->default(500)->nullable();
            $table->string('name', 255)->nullable(false);
            $table->integer('picture')->nullable();
            $table->integer('left_margin')->nullable();
            $table->integer('right_margin')->nullable();
            $table->integer('level')->default(1);
            $table->text('description')->nullable();
            $table->text('searchable_content')->nullable();
            $table->string('code', 255);
            $table->string('xml_id', 255)->nullable();
            $table->integer('detail_picture')->nullable();

	        $table->index('id_block', 'section_ix_block');
	        $table->index('parent_id', 'section_ix_parent_id');
	        $table->index('code', 'section_ix_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_sections');
    }
}
