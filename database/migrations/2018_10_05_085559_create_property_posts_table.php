<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_blog_post_property', function (Blueprint $table) {
	        $table->engine = "InnoDB";
			$table->integer('post_id')->unique();
			$table->text('downloads');
			$table->string('source_link', 255)->nullable();
			$table->string('source_title', 255)->nullable();
			$table->text('links_post');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_blog_post_property');
    }
}
