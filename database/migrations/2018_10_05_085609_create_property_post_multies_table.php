<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyPostMultiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_blog_post_property_multi', function (Blueprint $table) {
	        $table->engine = "InnoDB";
	        $table->integer('post_id')->unique();
	        $table->string('value', 255)->nullable();
	        $table->string('description', 255)->nullable();
	        $table->integer('value_num')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_blog_post_property_multi');
    }
}
