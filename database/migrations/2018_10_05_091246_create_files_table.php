<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_files', function (Blueprint $table) {
	        $table->engine = "InnoDB";
            $table->increments('id');
            $table->timestamps();
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->integer('size')->default(0);
            $table->string('size_format')->default('0');
            $table->string('content_type')->nullable();
            $table->string('sub_dir')->nullable();
            $table->string('name', 255)->nullable(false);
            $table->string('original_name', 255)->nullable();
            $table->string('hash', 255)->nullable(false);
            $table->string('description', 255)->nullable();
            $table->string('xml_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_files');
    }
}
