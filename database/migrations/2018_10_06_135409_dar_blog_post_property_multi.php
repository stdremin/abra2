<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DarBlogPostPropertyMulti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::drop('dar_blog_post_property_multi');

	    Schema::create('dar_blog_post_property_multi', function (Blueprint $table) {
		    $table->engine = "InnoDB";
	    	$table->increments('id');
		    $table->integer('post_id')->nullable(false)->index();
		    $table->string('value', 255)->nullable();
		    $table->string('description', 255)->nullable();
		    $table->integer('value_num')->nullable()->index();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
