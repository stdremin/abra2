<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dar_section_element', function (Blueprint $table) {
	        $table->engine = "InnoDB";
            $table->increments('id');
            $table->integer('section_id')->nullable(false);
            $table->integer('element_id')->nullable(false);

            $table->index(['section_id', 'element_id'],'ix_section_element');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dar_section_element');
    }
}
