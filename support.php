<?php
if (!function_exists('PR')){
	function PR($o)
	{
		$bt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
		$bt = $bt[0];
		$dRoot = $_SERVER["DOCUMENT_ROOT"];
		$dRoot = str_replace("/", "\\", $dRoot);
		$bt["file"] = str_replace($dRoot, "", $bt["file"]);
		$dRoot = str_replace("\\", "/", $dRoot);
		$bt["file"] = str_replace($dRoot, "", $bt["file"]);
		echo '<div style="font-size:9pt; color:#000; background:#fff; border:1px dashed #000; z-index: 700; position: relative">
			<div style="padding:3px 5px; background:#99CCFF; font-weight:bold;">File: '.$bt["file"].'
				['.$bt["line"].']
			</div>';
		echo '<pre style="padding:10px; background: #fff; text-align: left">';
		print_r($o);
		echo '</pre>
		</div>';
	}
}
if (!function_exists('PREXIT')){
	function PREXIT($var = false)
	{
		PR($var);
		exit;
	}
}